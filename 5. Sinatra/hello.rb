# First example using sinatra

require 'sinatra'

set :port, 8080
set :bind, '0.0.0.0'

get '/' do
  'Hello world!'
end

get '/no_content' do
  [406, { 'is-this-a-joke' => 'true' }, "No comment"]
end

get '/show' do
  @name = 'Jane'
  @feelings = ['Joy', 'Anger',
               'Sadness', 'Disgust',
               'Fear']
  @title = 'My Page'
  erb :show
end

get '/other' do
  @title = 'Other'
  erb :other_example
end

get '/name/:name' do
  "Hello #{ params[:name] }"
end
