require 'sequel'

DB = Sequel.connect('sqlite://books.db')

DB.drop_table? :books
DB.drop_table? :authors

DB.create_table :books do
  primary_key   :id
  String        :title
  foreign_key   :author_id, :authors
  Integer       :year
end

DB.create_table :authors do
  primary_key   :id
  String        :name
end

authors = DB[:authors]
books = DB[:books]

authors << { name: 'Ernest Cline' }    \
        << { name: 'George Orwell' }   \
        << { name: 'Michael Crichton'}

books << { title: 'Armada', author_id: 1, year: 2015 }               \
      << { title: 'Animal Farm', author_id: 2, year: 1945 }          \
      << { title: 'Congo', author_id: 3, year: 1980 }                \
      << { title: 'Jurassic Park', author_id: 3, year: 1990 }        \
      << { title: 'Nineteen Eighty-Four', author_id: 2, year: 1949 } \
      << { title: 'Ready Player One', author_id: 1, year: 2011 }

puts "Database created and populated."