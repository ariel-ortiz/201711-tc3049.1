require 'sequel'

DB = Sequel.connect('sqlite://models/books.db')

authors = DB[:authors]
books = DB[:books]

puts 'Retrieving Jurassic Park book info as a hash'
puts books[title: 'Jurassic Park']

puts

author_name = 'Ernest Cline'

id = authors[name: author_name][:id]
puts "ID for #{ author_name } = #{ id }"

puts
puts "Retrieving all the books by #{ author_name }"
books.where(author_id: id).each do |book|
  puts "  - \"#{ book[:title] }\" (#{ book[:year] })"
end

puts

puts "Deleting book with id = 3"
books.where(id: 3).delete

puts

puts "Update Michael Crichton's name"
authors.where(name: 'Michael Crichton').update(name: 'Mike Crichton')

puts

puts "Retrieving all books ordered by year"
books.order(:year).each do |book|
  author_id = book[:author_id]
  author = authors[id: author_id]
  puts "  - \"#{ book[:title] }\" by #{ author[:name] } (#{ book[:year] })"
end
