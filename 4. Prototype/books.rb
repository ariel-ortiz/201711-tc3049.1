# Example using the Prototype design pattern.

class Book
  attr_accessor :title, :author, :year, :isbn
  
  def clone
    c = super
    c.title = c.title.clone
    c.author = c.author.clone
    c
  end
end

b1 = Book.new
b1.title = "The Hunger Games"
b1.author = "Suzanne Collins"
b1.year = 2008
b1.isbn = 9780439023481

b2 = b1.clone
b2.title = "Catching Fire"
b2.author[0] = "Z"
p b2
p b1