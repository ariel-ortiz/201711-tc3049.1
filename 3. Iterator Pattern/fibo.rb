# An infinite Fibonacci generator (using the Enumerator class).

fibo = Enumerator.new do |yielder|
  a = 0
  b = 1
  loop do
    yielder << a
    a, b = b, a + b 
  end
end


p fibo.take(10) # Display the first 10 Fibonacci numbers.
