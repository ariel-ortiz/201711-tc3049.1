# A class example that shows how to define:
# - An instance initializer (constructor).
# - Attribute read-only accessors.
# - Overriden to_s, inspect, ==, <=> methods.

class Employee

  include Comparable

  def initialize(name, salary)
    @name = name
    @salary = salary
  end

  attr_reader :salary
  attr_accessor :name

  def to_s
    "I'm an employee, my name is #{ @name }, my salary is $#{ @salary }"
  end

  def inspect
    to_s
  end

  def ==(other)
    @salary == other.salary and @name == other.name
  end

  def <=>(other)
    @salary <=> other.salary
  end

end

# Testing our class:

e1 = Employee.new('John', 40_000)
puts e1
e2 = Employee.new('Doris', 100_000)
puts e2
e3 = Employee.new('John', 40_000)
puts e3.name
e3.name = 'Johnny'

# Use == to check object equality.
puts e1 == e3
e4 = e1
puts e1 == e4

# Use equal? to check object identity.
puts e1.equal?(e3)
puts e1.equal?(e4)

# Sort a collection of objects.
employee_array = [e1, e2, e3]
employee_array.sort!
p employee_array

# Using methods from the Comparable mix-in.
puts e1 < e2
puts e1 <= e3
puts e1.between?(e3, e2)

# Sorting using the Strategy design pattern.
array = [Employee.new('Jane', 10_000),
         Employee.new('Mary', 20_000),
         Employee.new('Garry', 15_000),
         Employee.new('Damien', 66_666),
         Employee.new('Dana', 25_000)]

p array.sort { |x, y| x.name <=> y.name }
p array.sort_by { |x| x.name.length }
