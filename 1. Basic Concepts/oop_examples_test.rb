# Unit test example.

require 'minitest/autorun'
require 'oop_examples'

class CoffeeTest < Minitest::Test
  
  def test_Coffee
    c = Coffee.new
    assert_equal 'This is a coffee.', c.to_s
    assert_equal 15, c.price
  end
  
  def test_Latte
    c = Latte.new
    assert_equal 'This is a coffee. With milk.', c.to_s
    assert_equal 40, c.price
  end
  
  #def test_other
  #  #assert_equal 3, 1+1
  #  #raise 'This is an exception'
  #  skip
  #  x = nil
  #  x.some_method
  #end

end