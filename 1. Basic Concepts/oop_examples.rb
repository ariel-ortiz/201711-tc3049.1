# Simple examples using basic OOP principles in Ruby.

class Coffee
  
#  def initialize
#    @myvar = '*'
#  end
  
  def to_s
    #@myvar.to_s + 'This is a coffee.'
    'This is a coffee.'
  end
  
  def price
    15
  end
  
end

class Latte < Coffee
  
  def to_s
    super + ' With milk.'
  end
  
  def price
    super + 25
  end
  
end

#c = Coffee.new
#puts c.to_s

